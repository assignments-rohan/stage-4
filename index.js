/* eslint-disable require-jsdoc */
/* eslint-disable object-curly-spacing */
/* eslint-disable max-len */
/* eslint-disable linebreak-style */
require('dotenv').config();
const path = require('path');
const jobmanager = require('./jobmanager');
const moment = require('moment');
const logger = require('./logger');
const fs = require("fs");
global.logger = logger;
const config = require('./custom/config.json');

let amqp = require('amqplib');

/* Message Format

##EOD message format
{
    "eventName":"eod",
    "data": {
       "fromdate":"06-12-2019",
       "todate":"06-12-2019"
    }
}
*/

const initjobmanager = async () => {
  // console.time("testrun");
  await jobmanager.init({database: config.database, logger: logger});
  await jobmanager.loadJobs(path.join(__dirname, 'jobs'));
  await jobmanager.loadCustomisations(path.join(__dirname, 'custom'));
  return jobmanager;
};

(async () => {
  try {
    await initjobmanager();
  } catch (error) {
    logger.error(`Index: Error while initializing job manager: ${error}`);
    throw error;
  }
  amqp
      .connect({
        protocol: (config.rabbitmq.secure) ? "amqps" : "amqp",
        hostname: config.rabbitmq.serverip,
        port: config.rabbitmq.port || null,
        username: config.rabbitmq.user,
        password: config.rabbitmq.password,
        locale: 'en_US',
        frameMax: 0,
        heartbeat: 0,
        vhost: '/',
      },(config.rabbitmq.secure && config.sslcert && config.sslkey)?{
        cert: fs.readFileSync(path.join(config.sslcert)),
        key: fs.readFileSync(path.join(config.sslkey)),
        rejectUnauthorized: false
      } : {})
      .then(function(conn) {
        process.once('SIGINT', function() {
          conn.close();
        });
        return conn.createChannel().then(function(ch) {
          let ok = ch.assertExchange(config.rabbitmq.exchange, 'topic', {durable: true });
          ok = ok.then(function() {
            return ch.assertQueue(config.rabbitmq.queue, { durable: true });
          });
          ok = ok.then(function() {
            return ch.bindQueue(config.rabbitmq.queue, config.rabbitmq.exchange, 'etl.#');
          });
          ok = ok.then(function() {
            return ch.prefetch(1);
          });
          ok = ok.then(function() {
            return ch.consume(config.rabbitmq.queue, doWork);
          });
          return ok.then(function() {
            logger.info('Index: [*] Waiting for messages. To exit press CTRL+C');
          });

          async function doWork(msg) {
            try {
              let obj = JSON.parse(msg.content.toString());
              logger.info('Index: Message received: ' + JSON.stringify(obj));
              if (obj.data.fromdate && obj.data.todate) {
                let fromdate = moment(obj.data.fromdate, globaldata.dateformat);
                let todate = moment(obj.data.todate, globaldata.dateformat);
                for (let m = moment(fromdate); m.isSameOrBefore(todate); m.add(1, 'days')) {
                  Object.assign(obj.data, { asondate: m.format(globaldata.dateformat) });
                  result = await jobmanager.triggerEvent(obj);
                  logger.info(`Index: All Jobs finished for date ${m.format(globaldata.dateformat)}`);
                }
              } else {
                logger.warn(`Index: No fromdate and todate specified`);
              }
              ch.ack(msg);
            } catch (error) {
              ch.ack(msg);
              logger.error('Index: Error occurred in dowork:' + error.stack);
            }
          }
        });
      })
      .catch((err) => logger.error('Index: Error occurred in processing:' + err.stack));
})();
process.on('SIGTERM', () => {
  console.info('SIGTERM signal received.');
});
