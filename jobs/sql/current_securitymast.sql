SELECT
    *
FROM
(
        SELECT
            curr_symbol,
            asset_class_name,
            compname,
            typedesc,
            securitysymbol,
            contractcode,
            security_desc,
            cftype,
            issdate,
            (
                matdate - issdate
            ) tenor,
            matdate,
            facevalue,
            interest,
            coupyear,
            nextipdate,
            holdingtype,
            ratingagency1,
            rating1,
            int_rating,
            ratingagency2,
            rating2,
            daycount,
            priority_type,
            tax_status,
            guarantee_type,
            guarantor,
            isinno,
            isin_name,
            isin_fv,
            iscapitalised,
            depository_name,
            risk_wt,
            listing_status,
            red_int_acc,
            benchmarkid,
            shut_period,
            first_odd_coupon,
            first_interest_date,
            last_odd_coupon,
            last_interest_date,
            series,
            redpremium,
            putcall,
            asset_class_code,
            scheme_code,
            catcode,
            frn,
            frn_flr,
            frn_cap,
            markup,
            frn_reset_unit,
            frn_compounding,
            reset_duration,
            frn_compounding_type,
            ai_day_count,
            objectivedesc,
            div_reinv,
            orientation,
            authorization_status,
            authorizedby,
            authorizationdatetime,
            deleted,
            catdesc,
            yield_calc,
            reporting_asset_class,
            min_amount,
            min_amt_mul,
            add_amt,
            add_amt_mul,
            bloomberg_ticker,
            reject_status,
            reject_reason
        FROM
            (
                SELECT
                    curr_symbol,
                    asset_class_master.asset_class_name,
                    COMPANYMASTER.COMPNAME AS COMPNAME,
                    SECUMAST.TYPEDESC AS TYPEDESC,
                    GLOBAL_SECURITY.SECURITYSYMBOL AS SECURITYSYMBOL,
                    GLOBAL_SECURITY.SECURITYSYMBOL AS CONTRACTCODE,
                    GLOBAL_SECURITY.SECURITY_DESC,
                    DECODE(
                        GLOBAL_SECURITY.FLOWTYPE,
                        0,
                        'MMDiscount',
                        1,
                        'MMIntPaying',
                        2,
                        'Regular',
                        3,
                        'Irregular Redemption',
                        4,
                        'Irregular CashFlows'
                    ) Cftype,
                    NVL(GLOBAL_SECURITY.ISSDATE, SYSDATE) AS ISSDATE,
                    CASE
                        WHEN asset_class_master.asset_class_code = 'EQ' THEN To_Date('')
                        ELSE NVL(GLOBAL_SECURITY.MATDATE, SYSDATE)
                    END AS matdate,
                    GLOBAL_SECURITY.FACEVALUE AS FACEVALUE,
                    GLOBAL_SECURITY.INTEREST AS INTEREST,
                    GLOBAL_SECURITY.COUPYEAR AS COUPYEAR,
                    CASE
                        WHEN asset_class_master.asset_class_code IN ('EQ', 'EQD', 'MF') THEN To_Date('')
                        ELSE NVL(GLOBAL_SECURITY.INTDUE1, SYSDATE)
                    END AS NEXTIPDATE,
                    GLOBAL_SECURITY.HOLDINGTYPE,
                    GLOBAL_SECURITY.AGENCY1_CODE AS RatingAgency1,
                    GLOBAL_SECURITY.RATING1 AS RATING1,
                    GLOBAL_SECURITY.INT_RATING AS INT_RATING,
                    GLOBAL_SECURITY.AGENCY2_CODE AS RatingAgency2,
                    GLOBAL_SECURITY.RATING2 AS RATING2,
                    CASE
                        WHEN asset_class_master.asset_class_code IN ('EQ', 'EQD', 'MF') THEN 'NA'
                        WHEN GLOBAL_SECURITY.intonactdays = 0
                        AND GLOBAL_SECURITY.cf_day_count = 0 THEN '30E/360'
                        WHEN GLOBAL_SECURITY.intonactdays = 1
                        AND GLOBAL_SECURITY.cf_day_count = 0 THEN 'Act/365'
                        WHEN GLOBAL_SECURITY.intonactdays = 1
                        AND GLOBAL_SECURITY.cf_day_count = 1 THEN 'Act/Act'
                        WHEN GLOBAL_SECURITY.intonactdays = 0
                        AND GLOBAL_SECURITY.cf_day_count = 1 THEN 'Act/360'
                        WHEN GLOBAL_SECURITY.intonactdays = 1
                        AND GLOBAL_SECURITY.cf_day_count = 3 THEN 'Act/360'
                        WHEN GLOBAL_SECURITY.intonactdays = 1
                        AND GLOBAL_SECURITY.cf_day_count = 2 THEN 'IBA-400'
                        WHEN GLOBAL_SECURITY.intonactdays = 4
                        AND GLOBAL_SECURITY.cf_day_count = 3 THEN 'Act364'
                        ELSE ''
                    END AS DayCount,
                    DECODE(
                        GLOBAL_SECURITY.Priority_Type,
                        1,
                        'Priority',
                        'Non  Priority'
                    ) Priority_type,
                    DECODE(
                        GLOBAL_SECURITY.Tax_Status,
                        0,
                        'Tax Free',
                        1,
                        'Taxable',
                        2,
                        'Tax Paid'
                    ) Tax_Status,
                    GLOBAL_SECURITY.GUARANTEE_TYPE AS GUARANTEE_TYPE,
                    GLOBAL_SECURITY.GUARANTOR AS GUARANTOR,
                    GLOBAL_SECURITY.RBI_SYMBOL AS IsinNo,
                    GLOBAL_SECURITY.ISIN_NAME AS ISIN_NAME,
                    DECODE(
                        GLOBAL_SECURITY.ASSET_CLASS_CODE,
                        'EQ',
                        GLOBAL_SECURITY.FACEVALUE,
                        GLOBAL_SECURITY.ISIN_FV
                    ) AS ISIN_FV,
                    DECODE(
                        GLOBAL_SECURITY.iscapitalised,
                        0,
                        'No',
                        1,
                        'YM',
                        2,
                        'YD',
                        'No'
                    ) IsCapitalised,
                    GLOBAL_SECURITY.DEPOSITORY_NAME AS DEPOSITORY_NAME,
                    GLOBAL_SECURITY.RISK_WT AS RISK_WT,
                    DECODE(
                        global_security.ASSET_CLASS_CODE,
                        'MF',
                        DECODE(global_security.listed, '1', 'Listed', 'Unlisted'),
                        DECODE(
                            GLOBAL_SECURITY.LISTING_STATUS,
                            '1',
                            'Listed',
                            'UnListed'
                        )
                    ) Listing_status,
                    DECODE(
                        GLOBAL_SECURITY.Red_Int_Acc,
                        0,
                        'Next IP Date',
                        'Redemption Date'
                    ) Red_int_acc,
                    GLOBAL_SECURITY.BENCHMARKID AS BENCHMARKID,
                    GLOBAL_SECURITY.SHUT_PERIOD AS SHUT_PERIOD,
                    DECODE(GLOBAL_SECURITY.first_odd_coupon, 1, 'Yes', 'No') First_Odd_coupon,
                    TO_CHAR(
                        DECODE(
                            GLOBAL_SECURITY.first_odd_coupon,
                            0,
                            'NA',
                            GLOBAL_SECURITY.FIRST_INTEREST_DATE
                        )
                    ) AS FIRST_INTEREST_DATE,
                    DECODE(last_odd_coupon, 1, 'Yes', 'No') last_odd_coupon,
                    DECODE(
                        NVL(GLOBAL_SECURITY.LAST_ODD_COUPON, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.LAST_INTEREST_DATE
                    ) AS LAST_INTEREST_DATE,
                    GLOBAL_SECURITY.SERIES AS SERIES,
                    GLOBAL_SECURITY.REDPREMIUM AS REDPREMIUM,
                    getputcall(global_security.securitysymbol, sysdate) Putcall,
                    GLOBAL_SECURITY.ASSET_CLASS_CODE AS ASSET_CLASS_CODE,
                    GLOBAL_SECURITY.SECURITYSYMBOL scheme_code,
                    IRDASECURITYCATEGORY.CATCODE,
                    DECODE(
                        NVL(TO_CHAR(GLOBAL_SECURITY.FRN), '0'),
                        '1',
                        'Yes',
                        'No'
                    ) FRN,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.FRN_FLR
                    ) FRN_FLR,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.FRN_CAP
                    ) FRN_CAP,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.MARKUP
                    ) MARKUP,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        DECODE(
                            GLOBAL_SECURITY.FRN_RESET_UNIT,
                            0,
                            'Days',
                            1,
                            'Months',
                            'Years'
                        )
                    ) FRN_RESET_UNIT,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.FRN_COMPOUNDING
                    ) FRN_COMPOUNDING,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.Frn_reset
                    ) reset_duration,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        DECODE(
                            GLOBAL_SECURITY.FRN_COMPOUNDING_TYPE,
                            1,
                            'Days-(Compounding on BD,Simple on HD)',
                            2,
                            'Months',
                            3,
                            'Years',
                            4,
                            'Days-(Compounding on All Days)'
                        )
                    ) FRN_COMPOUNDING_TYPE,
                    GLOBAL_SECURITY.AI_DAY_COUNT,
                    MFOBJECTIVE.OBJECTIVEDESC,
                    GLOBAL_SECURITY.DIV_REINV,
                    GLOBAL_SECURITY.ORIENTATION,
                    NVL(GLOBAL_SECURITY.AUTHORIZATION_STATUS, 0) AUTHORIZATION_STATUS,
                    GLOBAL_SECURITY.AUTHORIZEDBY,
                    TO_CHAR(
                        GLOBAL_SECURITY.AUTHORIZATIONDATETIME,
                        'MM-dd-yyyy hh24:mi:ss'
                    ) AUTHORIZATIONDATETIME,
                    GLOBAL_SECURITY.deleted,
                    IRDASECURITYCATEGORY.CATDESC,
                    'NA' yield_calc,
                    SECUMAST.reportingassetclass reporting_asset_class,
                    GLOBAL_SECURITY.min_amount,
                    GLOBAL_SECURITY.min_amt_mul,
                    GLOBAL_SECURITY.add_amt,
                    GLOBAL_SECURITY.add_amt_mul,
                    GLOBAL_SECURITY.bloomberg_ticker,
                    DECODE(
                        GLOBAL_SECURITY.reject_status,
                        1,
                        'Rejected',
                        0,
                        'Not Rejected'
                    ) reject_status,
                    GLOBAL_SECURITY.reject_reason
                FROM
                    GLOBAL_SECURITY,
                    MFOBJECTIVE,
                    SECUMAST,
                    COMPANYMASTER,
                    ASSET_CLASS_MASTER,
                    IRDASECURITYCATEGORY,
                    EQUITYDERIVATIVES
                WHERE
                    GLOBAL_SECURITY.IOBJ = MFOBJECTIVE.OBJCODE(+)
                    AND GLOBAL_SECURITY.SECURITY = COMPANYMASTER.COMPCODE(+)
                    AND GLOBAL_SECURITY.IRDACATCODE = IRDASECURITYCATEGORY.SYSCATCODE(+)
                    AND GLOBAL_SECURITY.TYPE = SECUMAST.TYPECODE
                    AND GLOBAL_SECURITY.ASSET_CLASS_CODE = ASSET_CLASS_MASTER.ASSET_CLASS_CODE
                    AND EQUITYDERIVATIVES.CONTRACTCODE(+) = GLOBAL_SECURITY.SECURITYSYMBOL
                    AND EQUITYDERIVATIVES.ORIGINALCODE IS NULL
                    AND ASSET_CLASS_MASTER.ASSET_CLASS_CODE IN ('MF', 'EQ', 'EQD', 'ALINV', 'CUD', 'COD', 'COMM')
                UNION
                SELECT
                    GLOBAL_SECURITY.curr_symbol,
                    asset_class_master.asset_class_name,
                    COMPANYMASTER.COMPNAME AS COMPNAME,
                    SECUMAST.TYPEDESC AS TYPEDESC,
                    DECODE(
                        asset_class_master.asset_class_code,
                        'MF',
                        GLOBAL_SECURITY.SECURITY_DESC,
                        GLOBAL_SECURITY.SECURITYSYMBOL
                    ) AS SECURITYSYMBOL,
                    GLOBAL_SECURITY.SECURITYSYMBOL AS CONTRACTCODE,
                    GLOBAL_SECURITY.SECURITY_DESC,
                    DECODE(
                        GLOBAL_SECURITY.FLOWTYPE,
                        0,
                        'MMDISCOUNT',
                        1,
                        'MMINTPAYING',
                        2,
                        'REGULAR',
                        3,
                        'IRREGULAR REDEMPTION',
                        4,
                        'IRREGULAR CASHFLOWS'
                    ) CFTYPE,
                    NVL(GLOBAL_SECURITY.ISSDATE, SYSDATE) AS ISSDATE,
                    GLOBAL_SECURITY.MATDATE AS MATDATE,
                    GLOBAL_SECURITY.FACEVALUE AS FACEVALUE,
                    GLOBAL_SECURITY.INTEREST AS INTEREST,
                    GLOBAL_SECURITY.COUPYEAR AS COUPYEAR,
                    NVL(GLOBAL_SECURITY.INTDUE1, SYSDATE) AS NEXTIPDATE,
                    GLOBAL_SECURITY.HOLDINGTYPE,
                    GLOBAL_SECURITY.AGENCY1_CODE AS RATINGAGENCY1,
                    GLOBAL_SECURITY.RATING1 AS RATING1,
                    GLOBAL_SECURITY.INT_RATING AS INT_RATING,
                    GLOBAL_SECURITY.AGENCY2_CODE AS RATINGAGENCY2,
                    GLOBAL_SECURITY.RATING2 AS RATING2,
                    CASE
                        WHEN GLOBAL_SECURITY.intonactdays = 0
                        AND GLOBAL_SECURITY.cf_day_count = 0 THEN '30E/360'
                        WHEN GLOBAL_SECURITY.intonactdays = 1
                        AND GLOBAL_SECURITY.cf_day_count = 0 THEN 'Act/365'
                        WHEN GLOBAL_SECURITY.intonactdays = 1
                        AND GLOBAL_SECURITY.cf_day_count = 1 THEN 'Act/Act'
                        WHEN GLOBAL_SECURITY.intonactdays = 0
                        AND GLOBAL_SECURITY.cf_day_count = 1 THEN 'Act/360'
                        WHEN GLOBAL_SECURITY.intonactdays = 1
                        AND GLOBAL_SECURITY.cf_day_count = 3 THEN 'Act/360'
                        WHEN GLOBAL_SECURITY.intonactdays = 1
                        AND GLOBAL_SECURITY.cf_day_count = 2 THEN 'IBA-400'
                        WHEN GLOBAL_SECURITY.intonactdays = 4
                        AND GLOBAL_SECURITY.cf_day_count = 3 THEN 'Act364'
                        ELSE ''
                    END DAYCOUNT,
                    DECODE(
                        GLOBAL_SECURITY.PRIORITY_TYPE,
                        1,
                        'PRIORITY',
                        'NON  PRIORITY'
                    ) PRIORITY_TYPE,
                    DECODE(
                        GLOBAL_SECURITY.Tax_Status,
                        0,
                        'Tax Free',
                        1,
                        'Taxable',
                        2,
                        'Tax Paid'
                    ) Tax_Status,
                    GLOBAL_SECURITY.GUARANTEE_TYPE AS GUARANTEE_TYPE,
                    GLOBAL_SECURITY.GUARANTOR AS GUARANTOR,
                    GLOBAL_SECURITY.RBI_SYMBOL AS ISINNO,
                    GLOBAL_SECURITY.ISIN_NAME AS ISIN_NAME,
                    GLOBAL_SECURITY.ISIN_FV AS ISIN_FV,
                    DECODE(
                        GLOBAL_SECURITY.ISCAPITALISED,
                        0,
                        'No',
                        1,
                        'YM',
                        2,
                        'YD',
                        'No'
                    ) ISCAPITALISED,
                    GLOBAL_SECURITY.DEPOSITORY_NAME AS DEPOSITORY_NAME,
                    GLOBAL_SECURITY.RISK_WT AS RISK_WT,
                    DECODE(
                        GLOBAL_SECURITY.LISTING_STATUS,
                        '1',
                        'Listed',
                        'Unlisted'
                    ) LISTING_STATUS,
                    DECODE(
                        GLOBAL_SECURITY.RED_INT_ACC,
                        0,
                        'NEXT IP DATE',
                        'REDEMPTION DATE'
                    ) RED_INT_ACC,
                    GLOBAL_SECURITY.BENCHMARKID AS BENCHMARKID,
                    GLOBAL_SECURITY.SHUT_PERIOD AS SHUT_PERIOD,
                    DECODE(GLOBAL_SECURITY.FIRST_ODD_COUPON, 1, 'Yes', 'No') FIRST_ODD_COUPON,
                    TO_CHAR(
                        DECODE(
                            GLOBAL_SECURITY.first_odd_coupon,
                            0,
                            'NA',
                            GLOBAL_SECURITY.FIRST_INTEREST_DATE
                        )
                    ) AS FIRST_INTEREST_DATE,
                    DECODE(GLOBAL_SECURITY.LAST_ODD_COUPON, 1, 'Yes', 'No') LAST_ODD_COUPON,
                    DECODE(
                        NVL(GLOBAL_SECURITY.LAST_ODD_COUPON, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.LAST_INTEREST_DATE
                    ) AS LAST_INTEREST_DATE,
                    GLOBAL_SECURITY.SERIES AS SERIES,
                    GLOBAL_SECURITY.REDPREMIUM AS REDPREMIUM,
                    GETPUTCALL(GLOBAL_SECURITY.SECURITYSYMBOL, SYSDATE) PUTCALL,
                    GLOBAL_SECURITY.ASSET_CLASS_CODE AS ASSET_CLASS_CODE,
                    GLOBAL_SECURITY.SECURITYSYMBOL SCHEME_CODE,
                    IRDASECURITYCATEGORY.CATCODE,
                    DECODE(
                        NVL(TO_CHAR(GLOBAL_SECURITY.FRN), '0'),
                        '1',
                        'Yes',
                        'No'
                    ) FRN,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.FRN_FLR
                    ) FRN_FLR,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.FRN_CAP
                    ) FRN_CAP,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.MARKUP
                    ) MARKUP,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        DECODE(
                            GLOBAL_SECURITY.FRN_RESET_UNIT,
                            0,
                            'Days',
                            1,
                            'Months',
                            'Years'
                        )
                    ) FRN_RESET_UNIT,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.FRN_COMPOUNDING
                    ) FRN_COMPOUNDING,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        GLOBAL_SECURITY.Frn_reset
                    ) reset_duration,
                    DECODE(
                        NVL(GLOBAL_SECURITY.FRN, 0),
                        0,
                        'NA',
                        DECODE(
                            GLOBAL_SECURITY.FRN_COMPOUNDING_TYPE,
                            1,
                            'Days-(Compounding on BD,Simple on HD)',
                            2,
                            'Months',
                            3,
                            'Years',
                            4,
                            'Days-(Compounding on All Days)'
                        )
                    ) FRN_COMPOUNDING_TYPE,
                    GLOBAL_SECURITY.AI_DAY_COUNT,
                    '' OBJECTIVEDESC,
                    '' DIV_REINV,
                    '' ORIENTATION,
                    NVL(GLOBAL_SECURITY.AUTHORIZATION_STATUS, 0) AUTHORIZATION_STATUS,
                    GLOBAL_SECURITY.AUTHORIZEDBY,
                    TO_CHAR(
                        GLOBAL_SECURITY.AUTHORIZATIONDATETIME,
                        'MM-dd-yyyy hh24:mi:ss'
                    ) AUTHORIZATIONDATETIME,
                    GLOBAL_SECURITY.deleted,
                    IRDASECURITYCATEGORY.CATDESC,
                    DECODE(secmast.yield_xirr, 0, 'Yield', 1, 'XIRR') Yield_calc,
                    SECUMAST.reportingassetclass reporting_asset_class,
                    GLOBAL_SECURITY.min_amount,
                    GLOBAL_SECURITY.min_amt_mul,
                    GLOBAL_SECURITY.add_amt,
                    GLOBAL_SECURITY.add_amt_mul,
                    GLOBAL_SECURITY.bloomberg_ticker,
                    DECODE(
                        GLOBAL_SECURITY.reject_status,
                        1,
                        'Rejected',
                        0,
                        'Not Rejected'
                    ) reject_status,
                    GLOBAL_SECURITY.reject_reason
                FROM
                    GLOBAL_SECURITY,
                    SECUMAST,
                    COMPANYMASTER,
                    ASSET_CLASS_MASTER,
                    IRDASECURITYCATEGORY,
                    secmast
                WHERE
                    GLOBAL_SECURITY.SECURITY = COMPANYMASTER.COMPCODE
                    AND GLOBAL_SECURITY.TYPE = SECUMAST.TYPECODE
                    AND GLOBAL_SECURITY.ASSET_CLASS_CODE = ASSET_CLASS_MASTER.ASSET_CLASS_CODE
                    AND GLOBAL_SECURITY.IRDACATCODE = IRDASECURITYCATEGORY.SYSCATCODE(+)
                    AND ASSET_CLASS_MASTER.ASSET_CLASS_CODE NOT IN ('MF', 'EQ', 'EQD', 'ALINV', 'CUD', 'COD', 'COMM')
                    AND secmast.securitysymbol = global_security.securitysymbol
                    AND global_security.matdate >= '12-11-2019'
            ) sec
        ORDER BY
            asset_class_name,
            ASSET_CLASS_CODE,
            SEC.SECURITYSYMBOL,
            TYPEDESC,
            MATDATE,
            NEXTIPDATE,
            INTEREST
    )