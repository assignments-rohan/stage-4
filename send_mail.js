const createTransport = require('nodemailer').createTransport;

function fetchMailer() {
    return {
        email: 'keshaun.labadie@ethereal.email',//enter your email id
        password: 'DwBTXQ1w1Njx9TcfWw',
        host: 'smtp.ethereal.email',
        port: 587,
        alias: 'Credence'
    };
}

// to => array of receivers
async function send_mails(to,subject,text,html) {
    try {
        to = to.join('');
        let sender = fetchMailer();
        if (sender) {
            let transporter = createTransport({
                host: sender.host,
                port: sender.port,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: sender.email,
                    pass: sender.password
                }
            });
            let mailOptions = {
                from: `"${sender.alias}" ${sender.email}`,
                to: to, // list of receivers
                subject: subject , // Subject line
                text: text, // plain text body
                html: html // html body
            };
            let info = await transporter.sendMail(mailOptions);
            await transporter.close();
            return console.log('Message sent: %s', info.messageId);
        } else throw 'Cannot send email : No mailer found';
    } catch (e) {
        return console.log(e);
    }
}

module.exports = send_mails;