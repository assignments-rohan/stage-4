// Get map function from `jobmanager` framework
const jobmgr = require("../jobmanager");
const updateDB = jobmgr.operators.updateDB;

/**
* Below are the three ways you can execute custom queries.
*/

// 1) passing query directly in string format.
module.exports = (pipeline, messagedata) => {
  pipeline.add(updateDB("updateBankbalances.sql", messagedata));
  return pipeline;
};