# ETL Flow

## products used

- RabbitMq
  - Exchange - `sysevents`
  - Queues and routing key
    1. historical - `hist`
    2. etljobs - `etl`
- NodeConsumer Framework
  - historical
  - opdconsumer
- ETL consumer Framework

## Flow

1. Front-end sends a msg to Exchange `sysevents` with routing-key: `hist` which goes to `historical` queue
   - Historical Msg format
    ```json
    // historical message format
    {
        "eventname": "eod",
        "data": {
            "fdate": "26-07-2019",
            "tdate": "30-07-2019",
            "portcode": "",
            "securitysymbol": ""
        }
    }
    ```
   - Intraday Msg format
    ```json
    // Intraday message format
        {
            "eventname": "intraday",
            "data": {
                "fdate": "26-07-2019",
                "tdate": "30-07-2019",
                "portcode": "",
                "securitysymbol": ""
            }
        }
    ```

2. NodeConsumer will read that data from historical queue and calls historical module script
   1. It will fire queries specified against `eventName` in `qryconfig.json`
    ```json
    // query config file format
    {
        "intraday": [
            "deallisting",
            "settleposition",
            "bankbalances"
            ],
        "eod": [
            "position",
            "deallisting",
            "settleposition",
            "bankbalances",
            "banktransaction",
            "accountentries",
            "limitstatus",
            "pas",
            "trailbalance",
            "interestaccrual",
            "ammortisation",
        ]
    }
    ```
   2. If no eventName specified then it will fire all queries
   3. After transferring data to historical tables, it will call etl process by sending a msg to exchange `sysevents` with routing-key: `etl` which goes to `etljobs` queue.
   4. The Message format differs, based on eventName
      1. If eventName is `eod`, etl msg format
        ```json
        // etl msg format
        {
            "eventName": "eod",
            "data": {
                "fromdate": "16/07/2018",
                "todate": "28/07/2018"
            }
        }
        ```
      2. If eventName `Intraday`, etl msg format
        ```json
        // intraday etl msg format
        {
            "eventName": "intraday",
            "data": {
                "fromdate": "16/07/2018",
                "todate": "28/07/2018"
            }
        }
        ```

3. ETL framework will read the msg from `etljobs` queue
   1. It will execute jobs, which are specified against `eventName` in `jobconfig.json`

    ```json
    //jobconfig.json format
    {
        "intraday": ["tradeposition"],
        "eod": [
            "tradeposition",
            "settleposition",
            "deallisting",
            "bankbalance",
            "banktransaction",
            "limitstatus",
            "nav",
            "trialbalance",
            "accountentries",
            "chartofaccounts",
            "companymaster",
            "securitymaster",
            "stockreco",
            "statewise_unit_data",
            "ordermandate",
            "pas",
            "amortisation",
            "interestaccrual"
        ]
    }

    ```
