const oracledb = require("oracledb");
const queryToCsv = require('../save_csv');

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
oracledb.autoCommit = true;

const mypw = "hr"; // set mypw to the hr schema password

function connect() {
  return (async function () {
    return await oracledb.getConnection({
      user: "hr",
      password: mypw,
      connectString: "localhost/orcl",
    });
  })();
}

async function get_emp_details() {
  let connection = null;
  try {
    connection = await connect();
    let query = `SELECT employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary  FROM EMPLOYEES`;
    const result = await connection.execute(query);
    queryToCsv(result);
    return result;
  } catch (err) {
    console.error(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
}

get_emp_details();

