const fs = require('fs');
module.exports = function (result){
    let csv_file = fs.createWriteStream('emp_details.csv');
    csv_file.on('error',(err) => console.log(err));
    
    let columns = [...result.metaData.map(el => el.name)];
    let rows = []
    for( let data of result.rows){ 
      let lineArray = [];
      for( let i = 0; i< columns.length; i++){
        lineArray.push(data[columns[i]])
      }
      rows.push(lineArray.join(','));
    }
    let csvString = [columns.join(','),'\n',rows.join('\n')].join('');
    csv_file.write(csvString);
    csv_file.end();
}